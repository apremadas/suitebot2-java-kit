package suitebot2.ai;

import suitebot2.game.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apremadas on 6/23/16.
 */
public class FunBotAI implements BotAi {
    enum ValidMoves {
        U,
        D,
        L,
        R,
        X;
    }
    private int myId;
    private Point myPosition;
    private List<Player> players;
    private GamePlan gamePlan;
    private int round;
    private boolean enemyFired = false;
    private int i=0;


    @Override
    public String initializeAndMakeMove(GameSetup gameSetup) {
        myId = gameSetup.aiPlayerId;
        gamePlan = gameSetup.gamePlan;
        players = gameSetup.players;
        round = 1;
        i=0;
        gameSetup.printGrid();
        setMyStartingPosition();

        return figureOutMove();
    }

    @Override
    public String makeMove(GameRound gameRound) {
        if(i<gamePlan.width) {
            i++;
            return ValidMoves.U.toString();

        }
        else
        {
            i=0;
            return ValidMoves.L.toString();
        }
    }

    private String figureOutMove()
    {
        return ValidMoves.U.toString();
    }
    private List figureOutDistance()
    {
        List distance=new ArrayList<>();
        for( Player player:players)
        {

        }
        return distance;
    }


    private void setMyStartingPosition()
    {
        int myIndex = players.indexOf(new Player(myId, null));
        myPosition = gamePlan.startingPositions.get(myIndex);
    }
}

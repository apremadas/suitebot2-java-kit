package suitebot2.ai;

import suitebot2.game.*;

import java.util.List;

public class SampleBotAi implements BotAi
{
	private int myId;
	private Point myPosition;
	private List<Player> players;
	private GamePlan gamePlan;
	private int round;
	private boolean enemyFired = false;

	@Override
	public String initializeAndMakeMove(GameSetup gameSetup)
	{
		myId = gameSetup.aiPlayerId;
		gamePlan = gameSetup.gamePlan;
		players = gameSetup.players;
		round = 1;

		setMyStartingPosition();

		return figureOutMove();
	}

	@Override
	public String makeMove(GameRound gameRound)
	{
		round++;

		enemyFired = false;
		for (PlayerMove playerMove : gameRound.moves)
			simulateMove(playerMove);

		return figureOutMove();
	}

	private void setMyStartingPosition()
	{
		int myIndex = players.indexOf(new Player(myId, null));
		myPosition = gamePlan.startingPositions.get(myIndex);
	}

	private void simulateMove(PlayerMove playerMove)
	{
		if (playerMove.playerId != myId && "FIRE".equals(playerMove.move))
			enemyFired = true;
	}

	private String figureOutMove()
	{
		if (enemyFired)
		{
			// If they have fired, we will fire too!
			return "FIRE";
		}
		else
		{
			if (round <= gamePlan.maxRounds / 2)
				return "WARM UP";
			else
				return "GO LEFT";
		}
	}
}

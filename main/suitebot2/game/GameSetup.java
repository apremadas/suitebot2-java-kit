package suitebot2.game;

import com.google.common.collect.ImmutableList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GameSetup
{
	public final String messageType = "setup";
	public final int aiPlayerId;
	public final List<Player> players;
	public final GamePlan gamePlan;
	public Map locationMap=new HashMap<>();
	public Map playerLocationMAp=new HashMap<String,Point>();
	public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
	{
		this.aiPlayerId = aiPlayerId;
		this.players = players;
		this.gamePlan = gamePlan;
	}

	@Override
	public String toString()
	{
		return "GameSetup{" +
				       "aiPlayerId=" + aiPlayerId +
				       ", players=" + players +
				       ", gamePlan=" + gamePlan +
				       '}';
	}

	public void printGrid()
	{
		populatePositionMap();
		Integer gridCurrentWeight=0;
		for (int i = 0; i < this.gamePlan.height; i++) {
			for (int j = 0; j < this.gamePlan.width; j++) {
				if(this.locationMap.containsKey(gridCurrentWeight))
				{
					System.out.print(this.locationMap.get(gridCurrentWeight));
				}
				else {
					System.out.print('X');
				}
				System.out.print("\t");
				gridCurrentWeight++;
			}
			System.out.print("\n");
		}
	}

	private void populatePositionMap() {
		Iterator iteratorPoint = gamePlan.startingPositions.iterator();
		Iterator iteratorPlayer = players.iterator();
		while (iteratorPoint.hasNext() && iteratorPlayer.hasNext()) {
			//this.locationMap.put(((Point) iteratorPoint.next()).getPointWeight(this.gamePlan.width, this.gamePlan.height), ((Player)iteratorPlayer.next()).id);
		}
	}

	private void populatePlayerPositionMap() {
		Iterator iteratorPoint = gamePlan.startingPositions.iterator();
		Iterator iteratorPlayer = players.iterator();
		while (iteratorPoint.hasNext() && iteratorPlayer.hasNext()) {
		//	this.playerLocationMAp.put( ((Player)iteratorPlayer.next()).id,(iteratorPoint.next()));
		}
	}
}

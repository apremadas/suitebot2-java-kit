package suitebot2.game;

import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

public class GamePlan
{
	public final int width;
	public final int height;
	public final List<Point> startingPositions;
	public final int maxRounds;

	public GamePlan(int width, int height, Collection<Point> startingPositions, int maxRounds)
	{
		this.width = width;
		this.height = height;
		this.startingPositions = ImmutableList.copyOf(startingPositions);
		this.maxRounds = maxRounds;
	}

	@Override
	public String toString()
	{
		return "GamePlan{" +
				       "width=" + width +
				       ", height=" + height +
				       ", startingPositions=" + startingPositions +
				       ", maxRounds=" + maxRounds +
				       '}';
	}
}

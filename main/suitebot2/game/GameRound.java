package suitebot2.game;

import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

public class GameRound {
	public final String messageType = "moves";
	public final List<PlayerMove> moves;

	public GameRound(Collection<PlayerMove> moves) {
		this.moves = ImmutableList.copyOf(moves);
	}

	public String toString() {
		return "GameRound{moves=" + this.moves + '}';
	}
}

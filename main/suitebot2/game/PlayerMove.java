package suitebot2.game;

public class PlayerMove
{
	public final String messageType = "moves";
	public final int playerId;
	public final String move;

	public PlayerMove(int playerId, String move)
	{
		this.playerId = playerId;
		this.move = move;
	}

	@Override
	public String toString()
	{
		return "PlayerMove{" +
				       "playerId=" + playerId +
				       ", move='" + move + '\'' +
				       '}';
	}
}

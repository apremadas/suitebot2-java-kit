package suitebot2.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeoutException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;

public class SimpleServerTest
{
	private static final int PORT = 4444;
	private Thread serverThread;

	@Before
	public void setUp() throws Exception
	{
		startServer();
	}

	@After
	public void tearDown() throws Exception
	{
		if (serverThread.isAlive())
			shutdownServer();

		waitForServerToShutDown(1000);
	}

	@Test
	public void testShuttingDown() throws Exception
	{
		assertTrue(serverThread.isAlive());
		shutdownServer();
		waitForServerToShutDown(1000);
		assertFalse(serverThread.isAlive());
	}

	@Test
	public void testUptimeRequest() throws Exception
	{
		Thread.sleep(1500);
		int upTime = Integer.valueOf(requestServerResponse(SimpleServer.UPTIME_REQUEST));
		assertThat(upTime, greaterThan(0));
	}

	@SuppressWarnings("SpellCheckingInspection")
	@Test
	public void testRequestResponse() throws Exception
	{
		assertThat(requestServerResponse("FooBar"), is("foobar"));
		assertThat(requestServerResponse("NextREQUEST"), is("nextrequest"));
	}

	private void startServer() throws InterruptedException
	{
		serverThread = new Thread(new SimpleServer(PORT, new ToLowerCaseConverter()));
		serverThread.start();
		Thread.sleep(100); // give the server thread some time to initialize
	}

	private void shutdownServer() throws IOException
	{
		requestServerResponse(SimpleServer.SHUTDOWN_REQUEST);
	}

	private void waitForServerToShutDown(int maxWaitTimeMilliseconds) throws InterruptedException, TimeoutException
	{
		int elapsed = 0;
		while (serverThread.isAlive())
		{
			if (elapsed > maxWaitTimeMilliseconds)
				throw new TimeoutException("timed out while waiting for the server to shut down");

			Thread.sleep(10);
			elapsed += 10;
		}
	}

	private String requestServerResponse(String request) throws IOException
	{
		try (
			Socket socket = new Socket("localhost", PORT);
			PrintWriter outputWriter = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))
		)
		{
			outputWriter.println(request);
			return inputReader.readLine();
		}
	}

	private static class ToLowerCaseConverter implements SimpleRequestHandler
	{
		@Override
		public String processRequest(String request)
		{
			return request.toLowerCase();
		}
	}
}

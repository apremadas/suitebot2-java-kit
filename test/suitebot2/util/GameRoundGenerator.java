//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package suitebot2.util;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import suitebot2.game.GameRound;
import suitebot2.game.Player;
import suitebot2.game.PlayerMove;

public class GameRoundGenerator {
    public static final List<String> POSSIBLE_MOVES = ImmutableList.of("FIRE", "RELOAD", "JUMP", "DUCK", "HOLD");

    public GameRoundGenerator() {
    }

    public static GameRound generateRandomGameRound(List<Player> players) {
        List playerMoves = (List)players.stream().map(GameRoundGenerator::generatePlayerMove).collect(Collectors.toList());
        return new GameRound(playerMoves);
    }

    private static PlayerMove generatePlayerMove(Player player) {
        String move = (String)POSSIBLE_MOVES.get((new Random()).nextInt(POSSIBLE_MOVES.size()));
        return new PlayerMove(player.id, move);
    }
}

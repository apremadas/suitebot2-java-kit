//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package suitebot2.util;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import suitebot2.game.GamePlan;
import suitebot2.game.GameSetup;
import suitebot2.game.Player;
import suitebot2.game.Point;

public class GameSetupGenerator {
    public static final int MIN_PLAN_SIDE_SIZE = 2;
    public static final int MAX_PLAN_SIDE_SIZE = 30;
    public static final int MIN_MAX_ROUNDS = 10;
    public static final int MAX_MAX_ROUNDS = 100;
    public static final int MIN_PLAYERS = 1;
    public static final int MAX_PLAYERS = 9;
    public static final int MIN_PLAYER_ID = 1;
    public static final int MAX_PLAYER_ID = 9;
    private Random random = new Random(System.currentTimeMillis());
    private int planWidth;
    private int planHeight;
    private int playerCount;

    public GameSetupGenerator() {
    }

    public GameSetup generateRandomGameSetup() {
        this.planWidth = this.randomInt(2, 30);
        this.planHeight = this.randomInt(2, 30);
        this.playerCount = this.randomInt(1, 9);
        List players = this.generatePlayers();
        int aiPlayerId = ((Player)players.get(this.randomInt(0, this.playerCount - 1))).id;
        int maxRounds = this.randomInt(10, 100);
        GamePlan gamePlan = new GamePlan(this.planWidth, this.planHeight, this.generateStartingPositions(), maxRounds);
        return new GameSetup(aiPlayerId, players, gamePlan);
    }

    private List<Player> generatePlayers() {
        ArrayList players = (ArrayList)IntStream.rangeClosed(1, 9).mapToObj((i) -> {
            return new Player(i, "Bot " + i);
        }).collect(Collectors.toCollection(ArrayList::new));
        Collections.shuffle(players);
        return players.subList(0, this.playerCount);
    }

    private List<Point> generateStartingPositions() {
        HashSet startingPositions = new HashSet();

        for(int i = 0; i < this.playerCount; ++i) {
            startingPositions.add(this.generateStartingPosition(startingPositions));
        }

        return ImmutableList.copyOf(startingPositions);
    }

    private Point generateStartingPosition(Set<Point> existingStartingPositions) {
        Point position;
        do {
            position = new Point(this.randomInt(0, this.planWidth - 1), this.randomInt(0, this.planHeight - 1));
        } while(existingStartingPositions.contains(position));

        return position;
    }

    private int randomInt(int minInclusive, int maxInclusive) {
        return this.random.nextInt(maxInclusive - minInclusive + 1) + minInclusive;
    }
}

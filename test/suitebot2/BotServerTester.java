//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package suitebot2;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.game.Player;
import suitebot2.util.GameRoundGenerator;
import suitebot2.util.GameSetupGenerator;

public class BotServerTester {
    public static final int SOFT_TIMEOUT_MS = 1000;
    public static final int HARD_TIMEOUT_MS = 200;
    private String botServerExecutable;
    private int port;

    public BotServerTester(String botServerExecutable) {
        this.botServerExecutable = botServerExecutable;
    }

    public BotServerTester() {
    }

    public boolean testBotServer(List<Integer> ports) {
        try {
            Iterator e = ports.iterator();

            int port;
            do {
                if (!e.hasNext()) {
                    return true;
                }

                port = ((Integer) e.next()).intValue();
            } while (this.testBotServer(port));

            return false;
        } catch (InterruptedException var4) {
            var4.printStackTrace();
            return false;
        }
    }
    public boolean testBotServer(String port) {
        try {
            return  this.testBotServer(Integer.parseInt(port));
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean testBotServer(int port) throws InterruptedException {
        if (isActiveListener(port) && botServerExecutable != null) {
            this.log("port " + port + " is already bound");
            return false;
        } else {
            boolean e;
            try {
                this.startBotServer(port);
                Thread.sleep(1000L);
                if (!this.isServerRunning()) {
                    throw new BotServerTester.BotServerError("failed to start the bot server");
                }

                this.testSetupAndMovesRequests();
                this.testUptimeRequest();
                this.testShutdownRequest();
                e = true;
            } catch (BotServerTester.BotServerError var7) {
                this.log(var7.getMessage());
                boolean var3 = false;
                return var3;
            } finally {
                if (this.isServerRunning()) {
                    this.shutDownBotServer();
                }

                this.log("");
            }

            return e;
        }
    }

    private void testUptimeRequest() throws BotServerTester.BotServerError, InterruptedException {
        this.log("");
        this.log("Testing UPTIME request...");

        try {
            int e = Integer.valueOf(this.requestServerResponse("UPTIME")).intValue();
            Thread.sleep(1000L);
            int uptime2 = Integer.valueOf(this.requestServerResponse("UPTIME")).intValue();
            int elapsed = uptime2 - e;
            if (elapsed != 1) {
                throw new BotServerTester.BotServerError("expected 1 second difference between the UPTIME responses; got: " + elapsed);
            }
        } catch (NumberFormatException var4) {
            throw new BotServerTester.BotServerError("error converting the UPTIME request response to integer: " + var4.getMessage(), var4);
        }
    }

    private void testSetupAndMovesRequests() throws BotServerTester.BotServerError {
        this.log("");
        this.log("Testing SETUP request...");
        GameSetup gameSetup = (new GameSetupGenerator()).generateRandomGameSetup();
        this.testSetupRequest(gameSetup);
        this.log("");
        this.log("Testing MOVES request...");
        int rounds = (new Random()).nextInt(gameSetup.gamePlan.maxRounds - 1);

        for (int i = 0; i < rounds; ++i) {
            this.testMovesRequest(gameSetup.players);
        }

    }

    private void testSetupRequest(GameSetup gameSetup) throws BotServerTester.BotServerError {
        String serializedGameSetup = (new Gson()).toJson(gameSetup);
        this.requestServerResponse(serializedGameSetup);
    }

    private void testMovesRequest(List<Player> players) throws BotServerTester.BotServerError {
        GameRound gameRound = GameRoundGenerator.generateRandomGameRound(players);
        String serializedGameRound = (new Gson()).toJson(gameRound);
        this.requestServerResponse(serializedGameRound);
    }

    private void testShutdownRequest() throws BotServerTester.BotServerError, InterruptedException {
        this.log("");
        this.log("Testing EXIT request...");
        this.requestServerResponse("EXIT");
        if (this.isBotServerDown()) {
            this.log("Bot server shut down successfully.");
        } else {
            throw new BotServerTester.BotServerError("bot server did not shut down after the EXIT request");
        }
    }

    private void startBotServer(int port) throws BotServerTester.BotServerError {
        this.port = port;
        if (botServerExecutable != null) {
            try {
                this.log("Starting the bot server on port " + port + "...");
                ProcessBuilder e = new ProcessBuilder(new String[]{this.botServerExecutable, Integer.toString(port)});
                e.redirectOutput(Redirect.INHERIT);
                e.redirectError(Redirect.INHERIT);
                e.start();
            } catch (IOException var3) {
                throw new BotServerTester.BotServerError("error while starting the bot server: " + var3.getMessage(), var3);
            }
        }
    }

    private void shutDownBotServer() throws InterruptedException {
        this.log("");
        this.log("Shutting down bot server...");

        try {
            this.requestServerResponse("EXIT");
        } catch (BotServerTester.BotServerError var5) {
            ;
        } finally {
            if (this.isBotServerDown()) {
                this.log("Bot server shut down successfully.");
            } else {
                this.log("Bot server shutdown failed. You may need to kill it manually.");
            }

        }

    }

    private boolean isBotServerDown() throws InterruptedException {
        for (int i = 0; i < 20; ++i) {
            if (!this.isServerRunning()) {
                return true;
            }

            Thread.sleep(100L);
        }

        return false;
    }

    private String requestServerResponse(String request) throws BotServerTester.BotServerError {
        try {
            Socket e = new Socket("localhost", this.port);
            Throwable var3 = null;

            String var11;
            try {
                PrintWriter outputWriter = new PrintWriter(e.getOutputStream(), true);
                Throwable var5 = null;

                try {
                    BufferedReader inputReader = new BufferedReader(new InputStreamReader(e.getInputStream()));
                    Throwable var7 = null;

                    try {
                        try {
                            e.setSoTimeout(1000);
                            Long e1 = Long.valueOf(System.currentTimeMillis());
                            this.log("request: " + request);
                            outputWriter.println(request);
                            String response = inputReader.readLine();
                            this.log("response: " + response);
                            Long responseTime = Long.valueOf(System.currentTimeMillis() - e1.longValue());
                            if (responseTime.longValue() > 200L) {
                                throw new BotServerTester.BotServerError(String.format("too long reponse time (%d ms); should be %d ms or less", new Object[]{responseTime, Integer.valueOf(200)}));
                            }

                            var11 = response;
                        } catch (IOException var61) {
                            throw new BotServerTester.BotServerError("error while communicating with the bot server: " + var61.getMessage(), var61);
                        }
                    } catch (Throwable var62) {
                        var7 = var62;
                        throw var62;
                    } finally {
                        if (inputReader != null) {
                            if (var7 != null) {
                                try {
                                    inputReader.close();
                                } catch (Throwable var60) {
                                    var7.addSuppressed(var60);
                                }
                            } else {
                                inputReader.close();
                            }
                        }

                    }
                } catch (Throwable var64) {
                    var5 = var64;
                    throw var64;
                } finally {
                    if (outputWriter != null) {
                        if (var5 != null) {
                            try {
                                outputWriter.close();
                            } catch (Throwable var59) {
                                var5.addSuppressed(var59);
                            }
                        } else {
                            outputWriter.close();
                        }
                    }

                }
            } catch (Throwable var66) {
                var3 = var66;
                throw var66;
            } finally {
                if (e != null) {
                    if (var3 != null) {
                        try {
                            e.close();
                        } catch (Throwable var58) {
                            var3.addSuppressed(var58);
                        }
                    } else {
                        e.close();
                    }
                }

            }

            return var11;
        } catch (IOException var68) {
            throw new BotServerTester.BotServerError("error while connecting to the bot server: " + var68.getMessage(), var68);
        }
    }

    private boolean isServerRunning() {
        return isActiveListener(this.port);
    }

    private void log(String message) {
        System.out.println(message);
    }

    private static boolean isActiveListener(int port) {
        try {
            Socket e = new Socket("localhost", port);
            Throwable var2 = null;

            boolean var3;
            try {
                var3 = e.isConnected();
            } catch (Throwable var13) {
                var2 = var13;
                throw var13;
            } finally {
                if (e != null) {
                    if (var2 != null) {
                        try {
                            e.close();
                        } catch (Throwable var12) {
                            var2.addSuppressed(var12);
                        }
                    } else {
                        e.close();
                    }
                }

            }

            return var3;
        } catch (IOException var15) {
            return false;
        }
    }

    private static class BotServerError extends Exception {
        public BotServerError(String message) {
            super(message);
        }

        public BotServerError(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

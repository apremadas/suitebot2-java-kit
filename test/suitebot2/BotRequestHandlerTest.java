package suitebot2;

import org.junit.Before;
import org.junit.Test;
import suitebot2.ai.BotAi;
import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.json.JsonUtilTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BotRequestHandlerTest
{
	private DummyBotAi dummyBotAi;
	private BotRequestHandler requestHandler;

	@Before
	public void setUp() throws Exception
	{
		dummyBotAi = new DummyBotAi();
		requestHandler = new BotRequestHandler(dummyBotAi);
	}

	@Test
	public void onSetupMessage_initializeShouldBeCalled() throws Exception
	{
		String aiMove = requestHandler.processRequest(JsonUtilTest.SETUP_MESSAGE);
		assertThat(dummyBotAi.makeMoveCalled, is(false));
		assertThat(aiMove, is("FIRSTMOVE"));
	}

	@Test
	public void onMovesMessage_makeMoveShouldBeCalled() throws Exception
	{
		String aiMove = requestHandler.processRequest(JsonUtilTest.MOVES_MESSAGE);
		assertThat(dummyBotAi.initializeAndMakeMoveCalled, is(false));
		assertThat(aiMove, is("NEXTMOVE"));
	}

	@Test
	public void onInvalidRequest_noAiMethodsShouldBeCalled() throws Exception
	{
		requestHandler.setSuppressErrorLogging(true);
		requestHandler.processRequest("invalid request");
		assertThat(dummyBotAi.initializeAndMakeMoveCalled, is(false));
		assertThat(dummyBotAi.makeMoveCalled, is(false));
	}

	class DummyBotAi implements BotAi
	{
		public boolean initializeAndMakeMoveCalled = false;
		public boolean makeMoveCalled = false;

		@Override
		public String initializeAndMakeMove(GameSetup gameSetup)
		{
			initializeAndMakeMoveCalled = true;
			return "FIRSTMOVE";
		}

		@Override
		public String makeMove(GameRound gameRound)
		{
			makeMoveCalled = true;
			return "NEXTMOVE";
		}
	}
}